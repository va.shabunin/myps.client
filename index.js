const net = require("net");
const EE = require("events");

const {composeFrame} = require("./compose");
const FrameParser = require("./Parser");

const MyClient = params => {
  let self = new EE();
  // init params
  let _params = {
    socketFile: `${process.cwd()}/myipc.sock`,
    debug: false,
    timeout: 5000
  };
  Object.assign(_params, params);

  // debug function
  let _debug = _params.debug;
  self.debug = (...args) => {
    if (_debug) {
      console.log(`./client:: ${args}`);
    }
  };

  // we store sent requests here
  let _timeout = _params.timeout;
  let requests = [];

  // socket
  let socket = new EE();


  let writeObject, writeString, write;

  const onIncomingData = data => {
    try {
      self.debug("incomig data");
      self.debug(data.toString());
      let dataObject = JSON.parse(data.toString());
      let method = dataObject.method;
      let payload = dataObject.payload;
      let response_id, topic, message, connection_id;
      let findById, foundRequests;
      // now depends on method and payload
      switch (method) {
        case "greeting":
          self.connection_id = payload;
          self.emit("connect", payload);
          break;
        case "message":
          // emit 'message', topic, message
          topic = payload.topic;
          message = payload.message;
          connection_id = payload.connection_id;
          self.emit("message", topic, message, connection_id);
          break;
        case "success":
          // find requests with given id and then resolve callback
          response_id = dataObject.response_id;
          findById = t => {
            return t.request_id === response_id;
          };
          foundRequests = requests.filter(findById);
          if (foundRequests.length > 0) {
            foundRequests.forEach(t => {
              t.callback(null, payload);
              // clear timeout handler
              clearTimeout(t.timeoutHandler);
            });
          } else {
            self.debug(`can't find request with id ${response_id}`);
          }
          break;
        case "error":
          // find requests with given id and then reject callback
          response_id = dataObject.response_id;
          findById = t => {
            return t.request_id === response_id;
          };
          foundRequests = requests.filter(findById);
          if (foundRequests.length > 0) {
            foundRequests.forEach(t => {
              t.callback(new Error(payload));
              // clear timeout handler
              clearTimeout(t.timeoutHandler);
            });
          } else {
            self.debug(`can't find request with id ${response_id}`);
          }
          break;
        default:
          break;
      }
      // TODO: parse, then emit
    } catch (e) {
      self.emit("error", e);
    }
  };
  // try to connect
  const tryToConnect = _ => {

    socket
      .removeAllListeners('connect')
      .removeAllListeners('error')
      .removeAllListeners('end')
      .removeAllListeners('close');

    socket = net.createConnection(_params.socketFile);

    socket.on("connect", _ => {
      self.debug("socket connected");

      // new frameParser instance
      let frameParser = new FrameParser();
      frameParser.on("data", onIncomingData);
      // pipe stream to bdsm frame parser
      socket.pipe(frameParser);
    });
    socket.on("close", _ => {
      self.emit("close");
    });
    socket.on("error", e => {
      self.emit("error", e);
    });
    socket.on("end", _ => {
      self.emit("end");
    });

  };

  tryToConnect();

  // write to socket funcs
  writeObject = dataToSend => {
    return socket.write(composeFrame(JSON.stringify(dataToSend)));
  };
  writeString = dataToSend => {
    return socket.write(composeFrame(dataToSend));
  };

  write = dataToSend => {
    switch (typeof dataToSend) {
      case "object":
        return writeObject(dataToSend);
      case "string":
        return writeString(dataToSend);
      default:
        break;
    }
  };

  // send functions
  // request-response model, refactor to ipc.js
  let sendRequest = (data, cb) => {
    if (!Object.prototype.hasOwnProperty.call(data, "method")) {
      throw new Error('Request should has "method" field.');
    }
    if (!Object.prototype.hasOwnProperty.call(data, "payload")) {
      throw new Error('Request should has "payload" field.');
    }
    if (typeof cb !== "function") {
      throw new Error("Please define callback for request");
    }
    // generate random request id
    let request_id = Math.floor(Math.random() * 0xffff);
    // compose request
    let request = {
      request_id: request_id,
      method: data.method,
      payload: data.payload
    };
    // push request information into requests object
    let requestInfo = {
      request_id: request_id,
      callback: cb
    };
    requests.push(requestInfo);

    // setTimeout to destroy request info and callback error TIMEOUT;
    requests[requests.length - 1].timeoutHandler = setTimeout(_ => {
      let findById = t => {
        return t.request_id === request_id;
      };
      // assuming request_id is unique
      let requestIndex = requests.findIndex(findById);
      // if we have request with given id
      if (requestIndex > -1) {
        //reject callback
        requests[requestIndex].callback(new Error("Request timeout"));
        // remove request from list
        requests.splice(requestIndex, 1);
      }
    }, _timeout);
    write(request);
  };

  self.subscribe = topic => {
    return new Promise((resolve, reject) => {
      self.debug(`subscribing to ${topic}`);
      let request = {};
      request.method = "subscribe";
      request.payload = topic;
      let cb = (err, payload) => {
        if (err) {
          reject(err);

          return;
        }
        resolve(payload);
      };
      sendRequest(request, cb);
    });
  };

  self.unsubscribe = topic => {
    return new Promise((resolve, reject) => {
      self.debug(`unsubscribing from ${topic}`);
      let request = {};
      request.method = "unsubscribe";
      request.payload = topic;
      let cb = (err, payload) => {
        if (err) {
          reject(err);

          return;
        }
        resolve(payload);
      };
      sendRequest(request, cb);
    });
  };

  self.publish = (topic, message) => {
    return new Promise((resolve, reject) => {
      self.debug(`publish to ${topic} message: ${message}`);
      let request = {};
      request.method = "publish";
      request.payload = {};
      request.payload.topic = topic;
      request.payload.message = message;
      let cb = (err, payload) => {
        if (err) {
          reject(err);

          return;
        }
        resolve(payload);
      };
      sendRequest(request, cb);
    });
  };

  self.reconnect = _ => {
    tryToConnect();
  };

  return self;
};

module.exports = MyClient;
