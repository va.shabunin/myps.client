# myps.client - client for my simple pub/sub implementation

## Example:

```js
let MyPubSub = require("myps.client");

let myPubSub = MyPubSub();

myPubSub.on("message", (topic, message, connection_id) => {
  console.log(`Message "${message}" from ${connection_id} to topic ${topic}`);
});

myPubSub.on("error", e => {
  console.log("aaargh", e.message);
  setTimeout(myPubSub.reconnect, 5000);
});

myPubSub.on("end", _ => {
  console.log("this is the end. the only end, my friend");
  setTimeout(myPubSub.reconnect, 5000);
});

myPubSub.on("connect", async connection_id => {
  console.log(`Connected as ${connection_id}`);
  try {
    await myPubSub.subscribe("friend");
    await myPubSub.publish("friend", "hello, drug");
    await myPubSub.publish("drug", "friend");
    await myPubSub.unsubscribe("friend");
    await myPubSub.unsubscribe("drug");
  } catch (e) {
    console.log("Errr::::", e.message);
  }
});
```
